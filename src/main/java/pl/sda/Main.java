package pl.sda;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        final String checkIfThisIsPalindrom = scanner.nextLine()
                .replace(" ", "")
                .toLowerCase();

        final StringBuilder inputStringBuilder = new StringBuilder(checkIfThisIsPalindrom);
        boolean isPalindrom = inputStringBuilder.reverse().toString().equals(checkIfThisIsPalindrom);

        System.out.println("Słowo " + (isPalindrom ? "jest" : "nie jest") + " palindromem");
    }
}
